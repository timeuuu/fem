clear all
clc
dbstop if error
%% Physical Parameters
epsilon0=(1/(36*pi))*1e-9;
mu0=4*pi*1e-7;
c=3e+8;
frequency=0.0926*1.5e+9;    %Hz
w = 2*pi*frequency;     %Angular frequency
k0 = 2*pi*frequency/c;   %wave number
epr = [1,1.77];
mur = [1,0.9999919934];
%epr = [1,1];
%mur = [1,1];
delta = @(m,n) (m==n);  
xdim = 1;ydim = 1;
isD = @(x,y) (abs(x-xdim)<=0.01)|(abs(y-ydim)<=0.01)|(abs(x+xdim)<=0.01)|(abs(y+ydim)<=0.01);
%% Mesh Generation
disp('Generating Mesh...')

load('cz_mesh.mat')
g = decsg(gd,sf,ns);
[p,e,t] = initmesh(g);
%[p,e,t] = refinemesh(g,p,e,t);

edge = [];
edge_lite = [];
num_node = size(p,2);
num_element = size(t,2);

for en  = 1:num_element
    edge(1,3*en-2)=t(1,en);
    edge(2,3*en-2)=t(2,en);
    %edge(3,3*en-2)=en;
    
    edge(1,3*en-1)=t(2,en);
    edge(2,3*en-1)=t(3,en);
    %edge(3,3*en-1)=en;
    
    edge(1,3*en)=t(3,en);
    edge(2,3*en)=t(1,en);
    %edge(3,3*en)=en;
end
for m = 1:size(edge,2)
    temp = edge(:,m);
    flag1= intersect(find(edge(2,1:m-1)==temp(1)),find(edge(1,1:m-1)==temp(2)));
    flag2= intersect(find(edge(1,1:m-1)==temp(1)),find(edge(2,1:m-1)==temp(2)));
    if isempty(flag1)&&isempty(flag2)
        edge_lite = [edge_lite,temp];
    end
end
num_edge = size(edge_lite,2);
node = p;
element = t;
edge = edge_lite;
Dmask = zeros(1,num_edge);
Nmask = zeros(1,num_node);
for i=1:num_edge
    Dmask(i)=isD(node(1,edge(1,i)),node(2,edge(1,i)))*isD(node(1,edge(2,i)),node(2,edge(2,i)));
end
for i = 1:num_node
    Nmask(i)=isD(node(1,i),node(2,i));
end
edge_mask = (1-Dmask);
node_mask = (1-Nmask);
disp('Mesh Generated!')
%% System Matrix Assembly
disp('Assembling System Matrix...')
% Initialization
A = zeros(num_edge+num_node,num_edge+num_node);
B = zeros(num_edge+num_node,num_edge+num_node);
Att = zeros(num_edge,num_edge);
Btt = zeros(num_edge,num_edge);
Btz = zeros(num_edge,num_node);
Bzt = zeros(num_node,num_edge);
Bzz = zeros(num_node,num_node);
% 
for e = 1:num_element
    Att_temp = zeros(3,3);Btt_temp = zeros(3,3);Btz_temp = zeros(3,3);Bzt_temp = zeros(3,3);Bzz_temp = zeros(3,3);
    %N = zeros(3,3);T = zeros(3,3);M = zeros(3,3);S = zeros(3,3);R = zeros(3,3);W = zeros(3,3);V = zeros(3,3);
    
    ep_r = epr(element(4,e));
    mu_r = mur(element(4,e));
    
    x(1)= node(1,element(1,e));y(1)=node(2,element(1,e));
    x(2)= node(1,element(2,e));y(2)=node(2,element(2,e));
    x(3)= node(1,element(3,e));y(3)=node(2,element(3,e));
    
    le(1)=sqrt((x(2)-x(1)).^2+(y(2)-y(1)).^2);
    le(2)=sqrt((x(3)-x(2)).^2+(y(3)-y(2)).^2);
    le(3)=sqrt((x(3)-x(1)).^2+(y(3)-y(1)).^2);
    
    a(1)=x(2)*y(3)-x(3)*y(2); b(1)=y(2)-y(3); c(1)=x(3)-x(2);
    a(2)=x(3)*y(1)-x(1)*y(3); b(2)=y(3)-y(1); c(2)=x(1)-x(3);
    a(3)=x(1)*y(2)-x(2)*y(1); b(3)=y(1)-y(2); c(3)=x(2)-x(1);
    
    delta_e = 0.5*(b(1)*c(2)-b(2)*c(1));
    
    A(1) = a(1)*b(2)-a(2)*b(1);A(2) = a(2)*b(3)-a(3)*b(2);A(3) = a(3)*b(1)-a(1)*b(3);
    B(1) = c(1)*b(2)-c(2)*b(1);B(2) = c(2)*b(3)-c(3)*b(2);B(3) = c(3)*b(1)-c(1)*b(3);
    C(1) = a(1)*c(2)-a(2)*c(1);C(2) = a(2)*c(3)-a(3)*c(2);C(3) = a(3)*c(1)-a(1)*c(3);
    D(1) = b(1)*c(2)-b(2)*c(1);D(2) = b(2)*c(3)-b(3)*c(2);D(3) = b(3)*c(1)-b(1)*c(3);
    
    index(1)= [intersect(find(edge(1,:)==element(1,e)),find(edge(2,:)==element(2,e))),...
               intersect(find(edge(2,:)==element(1,e)),find(edge(1,:)==element(2,e)))];
    flag(1,:) = 1-[isempty(intersect(find(edge(1,:)==element(1,e)),find(edge(2,:)==element(2,e)))),...
               isempty(intersect(find(edge(2,:)==element(1,e)),find(edge(1,:)==element(2,e))))];
    index(2)= [intersect(find(edge(1,:)==element(2,e)),find(edge(2,:)==element(3,e))),...
               intersect(find(edge(2,:)==element(2,e)),find(edge(1,:)==element(3,e)))];
    flag(2,:)=1-[isempty(intersect(find(edge(1,:)==element(2,e)),find(edge(2,:)==element(3,e)))),...
               isempty(intersect(find(edge(2,:)==element(2,e)),find(edge(1,:)==element(3,e))))];
    index(3)= [intersect(find(edge(1,:)==element(3,e)),find(edge(2,:)==element(1,e))),...
               intersect(find(edge(2,:)==element(3,e)),find(edge(1,:)==element(1,e)))];
    flag(3,:) = 1-[isempty(intersect(find(edge(1,:)==element(3,e)),find(edge(2,:)==element(1,e)))),...
                 isempty(intersect(find(edge(2,:)==element(3,e)),find(edge(1,:)==element(1,e))))];
    edge_order = flag(:,1)-flag(:,2);
    %% T(l,k)=Ni.*Nj
    T(1,1)=le(1)^2/(24*delta_e)*(le(2)^2+le(3)^2-b(1)*b(2)-c(1)*c(2));
    T(2,2)=le(2)^2/(24*delta_e)*(le(1)^2+le(3)^2-b(2)*b(3)-c(2)*c(3));
    T(3,3)=le(3)^2/(24*delta_e)*(le(1)^2+le(2)^2-b(1)*b(3)-c(1)*c(3));
    T(1,2)=le(1)*le(2)/(48*delta_e)*(b(2)*b(3)+c(2)*c(3)-le(3)^2-2*(b(1)*b(3)+c(1)*c(3))+b(1)*b(2)+c(1)*c(2)); T(2,1)=T(1,2);
    T(1,3)=le(1)*le(3)/(48*delta_e)*(b(1)*b(2)+c(1)*c(2)-2*(b(2)*b(3)+c(2)*c(3))-le(2)^2+b(1)*b(3)+c(1)*c(3)); T(3,1)=T(1,3);
    T(2,3)=le(2)*le(3)/(48*delta_e)*(b(1)*b(3)+c(1)*c(3)-le(1)^2-2*(b(1)*b(2)+c(1)*c(2))+b(2)*b(3)+c(2)*c(3)); T(3,2)=T(2,3);  
    %% W(l,k)
    W(1,1)=(le(1)./(12*delta_e))*((c(1)-c(2))*(-c(1))+(b(2)-b(1))*(b(1)));
    W(1,2)=(le(1)./(12*delta_e))*((c(1)-c(2))*(-c(2))+(b(2)-b(1))*(b(2)));
    W(1,3)=(le(1)./(12*delta_e))*((c(1)-c(2))*(-c(3))+(b(2)-b(1))*(b(3)));
    W(2,1)=(le(2)./(12*delta_e))*((c(2)-c(3))*(-c(1))+(b(3)-b(2))*(b(1)));
    W(2,2)=(le(2)./(12*delta_e))*((c(2)-c(3))*(-c(2))+(b(3)-b(2))*(b(2)));
    W(2,3)=(le(2)./(12*delta_e))*((c(2)-c(3))*(-c(3))+(b(3)-b(2))*(b(3)));
    W(3,1)=(le(3)./(12*delta_e))*((c(3)-c(1))*(-c(1))+(b(1)-b(3))*(b(1)));
    W(3,2)=(le(3)./(12*delta_e))*((c(3)-c(1))*(-c(2))+(b(1)-b(3))*(b(2)));
    W(3,3)=(le(3)./(12*delta_e))*((c(3)-c(1))*(-c(3))+(b(1)-b(3))*(b(3)));
    %% V(l,k)
    V = W';
    %% Local Matrix    
    for l=1:3
        for k = 1:3
            N(l,k) = (le(l)*le(k))/delta_e;
            S(l,k) = (b(l).*b(k)+c(l)*c(k))./(4*delta_e);
            R(l,k) = delta_e.*(1+delta(l,k))./12;

            Att_temp(l,k)=1/mu_r*N(l,k)-k0^2*ep_r*T(l,k);
            Btt_temp(l,k)=1/mu_r*T(l,k);
            Btz_temp(l,k)=1/mu_r*W(l,k);
            Bzt_temp(l,k)=1/mu_r*V(l,k);
            Bzz_temp(l,k)=1/mu_r*S(l,k)-k0^2*ep_r*R(l,k);
        end
    end
    %% Assembly into global matrix
    for l = 1:3
        for k = 1:3
            Att(index(l),index(k)) = Att(index(l),index(k)) + Att_temp(l,k).*edge_order(l).*edge_order(k); 
            Btt(index(l),index(k)) = Btt(index(l),index(k)) + Btt_temp(l,k).*edge_order(l).*edge_order(k);
            Btz(index(l),element(k,e)) = Btz(index(l),element(k,e)) + Btz_temp(l,k).*edge_order(l); 
            Bzt(element(l,e),index(k)) = Bzt(element(l,e),index(k)) + Bzt_temp(l,k).*edge_order(k);
            Bzz(element(l,e),element(k,e)) = Bzz(element(l,e),element(k,e)) + Bzz_temp(l,k); 
        end
    end    
end
%% Apply Directlet Boundary Condition
Att = Att.*(edge_mask'.*edge_mask);
Btt = Btt.*(edge_mask'.*edge_mask);
Btz = Btz.*(edge_mask'.*node_mask);
Bzt = Bzt.*(node_mask'.*edge_mask);
Bzz = Bzz.*(node_mask'.*node_mask);

Att(all(Att==0,2),:) = [];Att(:,all(Att==0,1))= [];
Btt(all(Btt==0,2),:) = [];Btt(:,all(Btt==0,1))= [];
Btz(all(Btz==0,2),:) = [];Btz(:,all(Btz==0,1))= [];
Bzt(all(Bzt==0,2),:) = [];Bzt(:,all(Bzt==0,1))= [];
Bzz(all(Bzz==0,2),:) = [];Bzz(:,all(Bzz==0,1))= [];

A = [Att,zeros(size(Btz));zeros(size(Bzt)),zeros(size(Bzz))];
B = [Btt,Btz;Bzt,Bzz];


disp('System Matrix Assembied ')
%% Solving Eigenvalue Problem
disp('Solving Eigenvalue Problem...')

mode_num = 12;
%[V,D] = eigs(B,A,mode_num,'sm');
[V,D] = eig(B,A);
disp('Eigenvalue Problem Solved!')


%beta = (-1./diag(D))./(k0^2);
%beta = sort(beta);
%beta = beta((beta>0));

beta = sort(diag(D));
beta = beta(beta<0);
(-1./beta)./(k0^2)
%% Visualization
%quiver(x,y,u,v)
isPlot = 1;

V = V(1:num_edge,:);
%V = V(:,find(diag(D)==((-1)/(beta(mode)*k0^2))));
x = 0.5*(node(1,edge(1,:)) + node(1,edge(2,:)));
y = 0.5*(node(2,edge(1,:)) + node(2,edge(2,:)));
u = zeros(1,num_edge);
v = zeros(1,num_edge);

mode = 5;
mode = find(diag(D)==beta(mode));

for i = 1:num_edge
    if Dmask(i) == 0
        E_temp = V(length(find(Dmask(1:i)==0)),mode);
        Exy(i)=E_temp;
    end
end
if isPlot
    cz_visualization;
end