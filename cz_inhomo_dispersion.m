load('InhomoData.txt')
k0 = InhomoData(1,:);
for i = 1:15
    T_mode(i,:)=InhomoData(i+1,:);
end
figure()
for i = 1:15
    plot(2*k0,T_mode(i,:),'k+')
    hold on
    plot(2*k0,T_mode(i,:))
end

title('Dispersion Diagram of Inhomogeneous Waveguide')
xlabel('k_0a')
ylabel('k_z^2/k_0^2')
axis([2.5,8.1,0,1.6])
grid on

