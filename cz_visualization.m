for e = 1:num_element
    %% Parameter Prep
    x(1)= node(1,element(1,e));y(1)=node(2,element(1,e));
    x(2)= node(1,element(2,e));y(2)=node(2,element(2,e));
    x(3)= node(1,element(3,e));y(3)=node(2,element(3,e));
    
    le(1)=sqrt((x(2)-x(1)).^2+(y(2)-y(1)).^2);
    le(2)=sqrt((x(3)-x(2)).^2+(y(3)-y(2)).^2);
    le(3)=sqrt((x(3)-x(1)).^2+(y(3)-y(1)).^2);
    
    a(1)=x(2)*y(3)-x(3)*y(2); b(1)=y(2)-y(3); c(1)=x(3)-x(2);
    a(2)=x(3)*y(1)-x(1)*y(3); b(2)=y(3)-y(1); c(2)=x(1)-x(3);
    a(3)=x(1)*y(2)-x(2)*y(1); b(3)=y(1)-y(2); c(3)=x(2)-x(1);
    
    delta_e = 0.5*(b(1)*c(2)-b(2)*c(1));
    
    A(1) = a(1)*b(2)-a(2)*b(1);A(2) = a(2)*b(3)-a(3)*b(2);A(3) = a(3)*b(1)-a(1)*b(3);
    B(1) = c(1)*b(2)-c(2)*b(1);B(2) = c(2)*b(3)-c(3)*b(2);B(3) = c(3)*b(1)-c(1)*b(3);
    C(1) = a(1)*c(2)-a(2)*c(1);C(2) = a(2)*c(3)-a(3)*c(2);C(3) = a(3)*c(1)-a(1)*c(3);
    D(1) = b(1)*c(2)-b(2)*c(1);D(2) = b(2)*c(3)-b(3)*c(2);D(3) = b(3)*c(1)-b(1)*c(3);
    
    index(1)= [intersect(find(edge(1,:)==element(1,e)),find(edge(2,:)==element(2,e))),...
               intersect(find(edge(2,:)==element(1,e)),find(edge(1,:)==element(2,e)))];
    flag(1,:) = 1-[isempty(intersect(find(edge(1,:)==element(1,e)),find(edge(2,:)==element(2,e)))),...
               isempty(intersect(find(edge(2,:)==element(1,e)),find(edge(1,:)==element(2,e))))];
    index(2)= [intersect(find(edge(1,:)==element(2,e)),find(edge(2,:)==element(3,e))),...
               intersect(find(edge(2,:)==element(2,e)),find(edge(1,:)==element(3,e)))];
    flag(2,:)=1-[isempty(intersect(find(edge(1,:)==element(2,e)),find(edge(2,:)==element(3,e)))),...
               isempty(intersect(find(edge(2,:)==element(2,e)),find(edge(1,:)==element(3,e))))];
    index(3)= [intersect(find(edge(1,:)==element(3,e)),find(edge(2,:)==element(1,e))),...
               intersect(find(edge(2,:)==element(3,e)),find(edge(1,:)==element(1,e)))];
    flag(3,:) = 1-[isempty(intersect(find(edge(1,:)==element(3,e)),find(edge(2,:)==element(1,e)))),...
                 isempty(intersect(find(edge(2,:)==element(3,e)),find(edge(1,:)==element(1,e))))];
    edge_order = flag(:,1)-flag(:,2);
    mask_order = (flag(:,1)-flag(:,2))'.*(flag(:,1)-flag(:,2));
    %% Calculate the value in center
    x_star = 1/3*(x(1)+x(2)+x(3));
    y_star = 1/3*(y(1)+y(2)+y(3));
    G_x = (1/(4*delta_e^2))*[le(1)*(A(1)+B(1)*y_star),...
                             le(2)*(A(2)+B(2)*y_star),...
                             le(3)*(A(3)+B(3)*y_star)];
    G_y = (1/(4*delta_e^2))*[le(1)*(C(1)+D(1)*x_star),...
                             le(2)*(C(2)+D(2)*x_star),...
                             le(3)*(C(3)+D(3)*x_star)];
    E_t = Exy(index); 
    
    x(e) = x_star;
    y(e) = y_star;
    u(e) = sum((G_x.*E_t).*(flag(:,1)-flag(:,2))');
    v(e) = sum((G_y.*E_t).*(flag(:,1)-flag(:,2))');
    
end
quiver(x,y,u,v)
title('Mode Distribution in Inhomogeneous Waveguide')
xlabel('x')
ylabel('y')
%quiver(x(abs(v)<=1e-2),y(abs(v)<=1e-2),u(abs(v)<=1e-2),v(abs(v)<=1e-2))
axis([-1,1,-1,1]);

%figure()
%[X,Y,Z]=griddata(x,y,sqrt(u.^2+v.^2)',linspace(min(x),max(x))',linspace(min(y),max(y)),'v4');
%surf(X,Y,Z,'EdgeColor','none');colorbar