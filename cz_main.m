clear all
clc
dbstop if error
%% Physical Parameters
epsilon0=(1/(36*pi))*1e-9;
mu0=4*pi*1e-7;
c=3e+8;
frequency=0.2*1.5e+8;    %Hz
w = 2*pi*frequency;     %Angular frequency
k0 = 2*pi*frequency/c;   %wave number
epr = [1,1];
mur = [1,1];
delta = @(m,n) (m==n);  
xdim = 1;ydim = 1;
isD = @(x,y) (abs(x-xdim)<=0.01)|(abs(y-ydim)<=0.01)|(abs(x+xdim)<=0.01)|(abs(y+ydim)<=0.01);
%% Mesh Generation
disp('Generating Mesh...')

load('cz_mesh.mat')
g = decsg(gd,sf,ns);
[p,e,t] = initmesh(g);

edge = [];
edge_lite = [];
num_node = size(p,2);
num_element = size(t,2);

for en  = 1:num_element
    edge(1,3*en-2)=t(1,en);
    edge(2,3*en-2)=t(2,en);
    %edge(3,3*en-2)=en;
    
    edge(1,3*en-1)=t(2,en);
    edge(2,3*en-1)=t(3,en);
    %edge(3,3*en-1)=en;
    
    edge(1,3*en)=t(3,en);
    edge(2,3*en)=t(1,en);
    %edge(3,3*en)=en;
end
for m = 1:size(edge,2)
    temp = edge(:,m);
    flag1= intersect(find(edge(2,1:m-1)==temp(1)),find(edge(1,1:m-1)==temp(2)));
    flag2= intersect(find(edge(1,1:m-1)==temp(1)),find(edge(2,1:m-1)==temp(2)));
    if isempty(flag1)&&isempty(flag2)
        edge_lite = [edge_lite,temp];
    end
end
num_edge = size(edge_lite,2);
node = p;
element = t;
edge = edge_lite;
Dmask = zeros(1,num_edge);
for i=1:num_edge
    %isD(node(1,edge(1,i)),node(2,edge(1,i)));
    %isD(node(1,edge(2,i)),node(2,edge(2,i)));
    Dmask(i)=isD(node(1,edge(1,i)),node(2,edge(1,i)))*isD(node(1,edge(2,i)),node(2,edge(2,i)));
end
mask = (1-Dmask)'.*(1-Dmask);
disp('Mesh Generated!')
%% System Matrix Assembly
disp('Assembling System Matrix...')


P = zeros(num_edge,num_edge);
Q = zeros(num_edge,num_edge);
X = zeros(num_edge,num_edge);

for e = 1:num_element
    P_temp = zeros(3,3);Q_temp = zeros(3,3);X_temp = zeros(3,3);
    N = zeros(3,3);T = zeros(3,3);M = zeros(3,3);
    
    ep_r = epr(element(4,e));
    mu_r = mur(element(4,e));
    
    x(1)= node(1,element(1,e));y(1)=node(2,element(1,e));
    x(2)= node(1,element(2,e));y(2)=node(2,element(2,e));
    x(3)= node(1,element(3,e));y(3)=node(2,element(3,e));
    
    le(1)=sqrt((x(2)-x(1)).^2+(y(2)-y(1)).^2);
    le(2)=sqrt((x(3)-x(2)).^2+(y(3)-y(2)).^2);
    le(3)=sqrt((x(3)-x(1)).^2+(y(3)-y(1)).^2);
    
    a(1)=x(2)*y(3)-x(3)*y(2); b(1)=y(2)-y(3); c(1)=x(3)-x(2);
    a(2)=x(3)*y(1)-x(1)*y(3); b(2)=y(3)-y(1); c(2)=x(1)-x(3);
    a(3)=x(1)*y(2)-x(2)*y(1); b(3)=y(1)-y(2); c(3)=x(2)-x(1);
    
    delta_e = 0.5*(b(1)*c(2)-b(2)*c(1));
    
    A(1) = a(1)*b(2)-a(2)*b(1);A(2) = a(2)*b(3)-a(3)*b(2);A(3) = a(3)*b(1)-a(1)*b(3);
    B(1) = c(1)*b(2)-c(2)*b(1);B(2) = c(2)*b(3)-c(3)*b(2);B(3) = c(3)*b(1)-c(1)*b(3);
    C(1) = a(1)*c(2)-a(2)*c(1);C(2) = a(2)*c(3)-a(3)*c(2);C(3) = a(3)*c(1)-a(1)*c(3);
    D(1) = b(1)*c(2)-b(2)*c(1);D(2) = b(2)*c(3)-b(3)*c(2);D(3) = b(3)*c(1)-b(1)*c(3);
    
    index(1)= [intersect(find(edge(1,:)==element(1,e)),find(edge(2,:)==element(2,e))),...
               intersect(find(edge(2,:)==element(1,e)),find(edge(1,:)==element(2,e)))];
    flag(1,:) = 1-[isempty(intersect(find(edge(1,:)==element(1,e)),find(edge(2,:)==element(2,e)))),...
               isempty(intersect(find(edge(2,:)==element(1,e)),find(edge(1,:)==element(2,e))))];
    index(2)= [intersect(find(edge(1,:)==element(2,e)),find(edge(2,:)==element(3,e))),...
               intersect(find(edge(2,:)==element(2,e)),find(edge(1,:)==element(3,e)))];
    flag(2,:)=1-[isempty(intersect(find(edge(1,:)==element(2,e)),find(edge(2,:)==element(3,e)))),...
               isempty(intersect(find(edge(2,:)==element(2,e)),find(edge(1,:)==element(3,e))))];
    index(3)= [intersect(find(edge(1,:)==element(3,e)),find(edge(2,:)==element(1,e))),...
               intersect(find(edge(2,:)==element(3,e)),find(edge(1,:)==element(1,e)))];
    flag(3,:) = 1-[isempty(intersect(find(edge(1,:)==element(3,e)),find(edge(2,:)==element(1,e)))),...
                 isempty(intersect(find(edge(2,:)==element(3,e)),find(edge(1,:)==element(1,e))))];
    mask_order = (flag(:,1)-flag(:,2))'.*(flag(:,1)-flag(:,2));
    %% calculate local matrix    
    for l=1:3
        for k = 1:3
            T(l,k) = (le(l)*le(k)/(8*delta_e^3))*...
                     sum(sum(...
                     [A(k)+B(k)*y(3);-B(k)*b(2);B(k)*b(1)].*...
                     [A(l)+B(l)*y(3),-B(l)*b(2),B(l)*b(1)].*...
                     [1/2,1/6,1/6;1/6,1/12,(1)/24;1/6,(1)/24,1/12]...
                     +...
                     [C(k)+D(k)*x(3);D(k)*c(2);-D(k)*c(1)].*...
                     [C(l)+D(l)*x(3),D(l)*c(2),-D(l)*c(1)].*...
                     [1/2,1/6,1/6;1/6,1/12,1/24;1/6,1/24,1/12]));
            N(l,k) = (le(l)*le(k)/(16*delta_e^3))*(D(l)-B(l))*(D(k)-B(k));
            M(l,k) = (le(l)*le(k)/(8*delta_e^3))*...
                     sum(sum(...
                     [C(k)+D(k)*x(3);D(k)*c(2);-D(k)*c(1)].*...
                     [A(l)+B(l)*y(3),-B(l)*b(2),B(l)*b(1)].*...
                     [1/2,1/6,1/6;1/6,1/12,1/24;1/6,1/24,1/12]...
                     -...
                     [A(k)+B(k)*y(3);-B(k)*b(2);B(k)*b(1)].*...
                     [C(l)+D(l)*x(3),D(l)*c(2),-D(l)*c(1)].*...
                     [1/2,1/6,1/6;1/6,1/12,1/24;1/6,1/24,1/12]));
            
                 
            X_temp(l,k)= - M(l,k);
            P_temp(l,k)= -(1/k0).*((1/(ep_r))*N(l,k)-(k0^2)*(mu_r)*T(l,k));
            Q_temp(l,k)= +(1/k0).*((1/(mu_r))*N(l,k)-(k0^2)*(ep_r)*T(l,k));
        end
    end
    %% Assembly into global matrix
    for l = 1:3
        for k = 1:3
            X(index(l),index(k)) = X(index(l),index(k)) + X_temp(l,k).*mask_order(l,k);
            P(index(l),index(k)) = P(index(l),index(k)) + P_temp(l,k).*mask_order(l,k);
            Q(index(l),index(k)) = Q(index(l),index(k)) + Q_temp(l,k).*mask_order(l,k);
        end
    end    
end
%% Apply Directlet Boundary Condition
%X = X.*mask;
%P = P.*mask
X = X.*(ones(num_edge).*(1-Dmask));
P = P; 
Q = Q.*mask;

X(all(X==0,2),:) = [];X(:,all(X==0,1))= [];
P(all(P==0,2),:) = [];P(:,all(P==0,1))= [];
Q(all(Q==0,2),:) = [];Q(:,all(Q==0,1))= [];

disp('System Matrix Assembied ')

%% Solving Eigenvalue Problem
disp('Solving Eigenvalue Problem...')

mode_num = 6;
[V,D] = eigs(Q,X'*inv(P)*X,mode_num,'sm');
%[V,D] = eigs(-P,X*inv(Q)*X',mode_num,'sm');

disp('Eigenvalue Problem Solved!')
diag(D)%./(k0^2)

%% Visualization
%quiver(x,y,u,v)
%isPlot = 0;

x = 0.5*(node(1,edge(1,:)) + node(1,edge(2,:)));
y = 0.5*(node(2,edge(1,:)) + node(2,edge(2,:)));
u = zeros(1,num_edge);
v = zeros(1,num_edge);
mode = 5;
for i = 1:num_edge
    if Dmask(i) == 0
        E_temp = V(length(find(Dmask(1:i)==0)),mode);
        Exy(i)=E_temp;
    end
end

cz_visualization;